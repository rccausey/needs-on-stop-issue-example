# needs-on-stop-issue-example

This shows an example of the review app pipeline getting stuck when the needs keyword is used to make a job depend on a job that is an on_stop target.